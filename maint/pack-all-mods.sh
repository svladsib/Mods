#!/bin/bash
set -eEuo pipefail

cd "$(dirname "$0")"/../mods

for f in ./*
do
	if [[ -d "$f" ]]
	then
		rm -f "$f".zip
		7z a "$f".zip "$f"
	fi
done
