# Community Modding System for ΔV: Rings of Saturn

This document describes the modding system (loader and API) included with the game ΔV: Rings of Saturn.

Please note that this modding system is **unsupported** by the game's developers, and is maintained by the community.

## Using Mods

**WARNING**: Mods contain arbitrary code, which will be executed with the same privileges as the game itself. An evil mod may contain malware which can take over your computer, and destroy or steal your data. Do not run mods from untrusted sources.

Mods are distributed as `.zip` files. One `.zip` file contains one mod.

To install a mod, go to the game's installation directory (the directory containing `Delta-V.pck`), and create a new directory called `mods`. Place the mod `.zip` files in this `mods` directory. Do not unpack the `.zip` files.

To run the game with mods, run it with the `--enable-mods` command-line parameter. If using Steam, you can add the parameter in the game's properties &rarr; General &rarr; Launch Options.

Achievements will be disabled when using mods.

## Writing Mods

### Mod structure

There should not be any files in the root directory of the `.zip` file - instead, all of the mod's files should be in a directory, named after the mod. For example, a mod which adds a heavy Cothon variant, distributed as `HeavyCothon.zip`, should have all its files in a directory called `HeavyCothon`. This ensures that the mod does not accidentally interfere with parts of the game or other mods.

At the moment, the only "special" files inside the ZIP file are files called `ModMain.gd`. The mod loader will load and instantiate all such files as scripts; it is then the duty of these scripts to use the mod loader API (see below) to install hooks or load additional resources. `ModMain.gd` scripts should extend `Node`.

`ModMain.gd` scripts may optionally declare a top-level numeric constant named `MOD_PRIORITY`. If present, it controls the order in which `ModMain.gd` scripts are executed - from lowest to highest priority. If the constant is not present, the default priority is 0.

Files which override or extend game scripts should, by convention, have the same path as the game script; so, if the mod HeavyCothon wants to expand the game script `ships/Shipyard.gd`, it should have the extending script at `HeavyCothon/ships/Shipyard.gd`. Following this convention will make it easier to collaborate on mods, and may help with relative path issues.

### Initialization

`ModMain.gd` scripts are initialized in two important steps, each being customizable using a Godot hook function:

#### `_init`

The `_init` function looks like this:

```gdscript
func _init(modLoader = ModLoader):
	Debug.l("Initializing my mod")
```

At the time when `_init` is called, singletons (except `Tool` and `Debug`) are not available; this includes `ModLoader`, so the mod loader passes itself as a parameter to `_init`.

#### `_ready`

The `_ready` hook looks like usual:

```gdscript
func _ready():
    Debug.l("My mod is ready")
```

#### `_init` or `_ready`?

`_init` is appropriate for installing script extensions. Specifically, singletons (autoloads) can only be extended from `_init`, before they are intantiated.

At the time when `_ready` is called, all singletons are already available. Any initialization code which needs to access e.g. `CurrentGame` is better suited for `_ready`.

Godot scene tree manipulation is better suited for `_ready` as well.

### Modding API

*ΔV: Rings of Saturn* currently provides neither an official nor a stable modding API. Instead, the API provided here aims to allow selectively patching game code and resources as granularly as possible (within GDScript limitations).

As such, the functions listed below serve the roles of helpers and orchestration, and though their use is not *necessary*, it is nevertheless recommended.

The functions can be invoked as `ModLoader.functionNameHere(...)`, except in `_init`, where they must be invoked via the passed parameter (`modLoader.functionNameHere(...)`).

#### `installScriptExtension`

Given a path to a script which extends some game script, replaces the original game script with the given extension script.

```gdscript
func installScriptExtension(
	# res:// path to the extending script
	childScriptPath:String
)
```

See the section "Script inheritance" below for more details.

#### `addTranslationsFromCSV`

Reads a CSV file and adds the contents as language strings.

The CSV file should have one header line. The first column of the header line is ignored; the second and further columns of the header line should contain the [locales](https://docs.godotengine.org/en/stable/tutorials/i18n/locales.html) of the strings below.

Lines past the first line contain language strings. The first column is the untranslated string; columns after that are the translations, corresponding to the locales in the header line.

The syntax of the CSV file is that of [`File.get_csv_line`](https://docs.godotengine.org/en/stable/classes/class_file.html#class-file-method-get-csv-line).

```gdscript
func addTranslationsFromCSV(
	# res:// path to the CSV file.
	csvPath: String
)
```

#### `addTranslationsFromJSON`

Reads a JSON file and adds the contents as language strings.

The CSV file should have one header line. The first column of the header line is ignored; the second and further columns of the header line should contain the [locales](https://docs.godotengine.org/en/stable/tutorials/i18n/locales.html) of the strings below.

Lines past the first line contain language strings. The first column is the untranslated string; columns after that are the translations, corresponding to the locales in the header line.

The syntax of the CSV file is that of [`File.get_csv_line`](https://docs.godotengine.org/en/stable/classes/class_file.html#class-file-method-get-csv-line).

```gdscript
func addTranslationsFromCSV(
	# res:// path to the CSV file.
	csvPath: String
)
```

#### `appendNodeInScene`

Helper function: create and add a node to a instanced scene.

```gdscript
func appendNodeInScene(
	# an instanced scene of a PackedScene you want to replace
	modifiedScene:Scene, 

	# name of the new node
	nodeName:String = "", 

	# path to the parent node in the PackedScene
	nodeParent = null, 

	# path used to instanciate the node
	instancePath:String = "", 

	# node visibility state
	isVisible:bool = true
)
```

#### `saveScene`

Helper function: save the scene as a PackedScene, overwriting Godot's cache if needed.

```gdscript
func saveScene(
	# an instanced scene of a PackedScene you want to replace
	modifiedScene:Scene, 

	# path to the PackedScene
	scenePath:String
)
```

## Technical Architecture

### Overview

The mod loader does two things:

1. Plug the mod `.zip` file into the Godot resource virtual filesystem using `ProjectSettings.load_resource_pack`

2. Search for file entries called `ModMain.gd` in the `.zip` file. For every such entry found, a script is loaded and instantiated from the equivalent Godot resource virtual filesystem path (`res://...`). The script instances are added as children nodes of the `ModLoader` script.

### Script inheritance

In order to allow modifying game functions without copying the contents of the functions or entire scripts in mod code, this system makes use of inheritance for hooking. We exploit the fact that all Godot scripts are also classes and may extend other scripts in the same way how a class may extend another class; this allows mods to include script extensions, which extend some standard game script and selectively override some methods.

For example, this simple script extension adds a ship for sale in the Dealer's ship list:

```gdscript
# Our base script is the original game script.
extends "res://CurrentGame.gd"

# This overrides the method with the same name from the game's CurrentGame.gd script.
func getShipsAvailableForSale():
	# Calling the base method will call the original game method:
	var ships = .getShipsAvailableForSale()

	# We can now modify the result:
	ships.append(createShipInstanceWithCache("COTHON", 24*3600*7, 0, true))

	# Return the result, returning control back to the game code.
	return ships
```

To install this script extension, call `modLoader.installScriptExtension` from your mod's `ModMain.gd`, in `_init`:

```gdscript
extends Node

func _init(modLoader = ModLoader):
	modLoader.installScriptExtension('res://MyMod/CurrentGame.gd')
```

### Inheritance chaining

To allow multiple mods to customize the same base script, `installScriptExtension` takes care to enable inheritance chaining. In practice, this is transparent to mods, and things should "just work".

If a script given to `installScriptExtension` extends from a game script which is already extended, then it will be extended from the last child script. For example, if we have two mods with a `extends "res://CurrentGame.gd"` script, then the first mod's script will extend from the game's `CurrentGame`, but after this `"res://CurrentGame.gd"` will point at the extended script, so the second mod's script will extend from the first mod's script instead.

## Changelog

- v???.? - 2021-06-?? - Initial release

  The initial ModLoader implementation by Harrygiel allowed wholesale overriding of game resources, and included some helpers to manipulate scene files and add language strings. There was no composability support.

- v60?.? - 2022-05-?? - Second-generation release

  An overhaul of the modding system and approach used, aiming to make mods more composable and future-proof by removing the need to duplicate parts of game code. Introduces the script extensions mechanism.

  All prior mods require modifications to be compatible with the new system.
