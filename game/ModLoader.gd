# ModLoader - A mod loader for GDScript
#
# Written in 2021 by harrygiel <harrygiel@gmail.com>,
# in 2021 by Mariusz Chwalba <mariusz@chwalba.net>,
# in 2022 by Vladimir Panteleev <git@cy.md>
#
# To the extent possible under law, the author(s) have
# dedicated all copyright and related and neighboring
# rights to this software to the public domain worldwide.
# This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public
# Domain Dedication along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Node

var areModsEnabled = false

func _init():
	for arg in OS.get_cmdline_args():
		if arg == "--enable-mods":
			areModsEnabled = true

	if !areModsEnabled:
		return

	Debug.l("ModLoader: Loading mods...")
	_loadMods()
	Debug.l("ModLoader: Done loading mods.")

	Debug.l("ModLoader: Initializing mods...")
	_initMods()
	Debug.l("ModLoader: Done initializing mods.")


var _modZipFiles = []

func _loadMods():
	var gameInstallDirectory = OS.get_executable_path().get_base_dir()
	if OS.get_name() == "OSX":
		gameInstallDirectory = gameInstallDirectory.get_base_dir().get_base_dir().get_base_dir()
	var modPathPrefix = gameInstallDirectory.plus_file("mods")

	var dir = Directory.new()
	if dir.open(modPathPrefix) != OK:
		Debug.l("ModLoader: Can't open mod folder %s." % modPathPrefix)
		return
	if dir.list_dir_begin() != OK:
		Debug.l("ModLoader: Can't read mod folder %s." % modPathPrefix)
		return

	while true:
		var fileName = dir.get_next()
		if fileName == '':
			break
		if dir.current_is_dir():
			continue
		var modFSPath = modPathPrefix.plus_file(fileName)
		var modGlobalPath = ProjectSettings.globalize_path(modFSPath)
		if !ProjectSettings.load_resource_pack(modGlobalPath, true):
			Debug.l("ModLoader: %s failed to load." % fileName)
			continue
		_modZipFiles.append(modFSPath)
		Debug.l("ModLoader: %s loaded." % fileName)
	dir.list_dir_end()


# Load and run any ModMain.gd scripts which were present in mod ZIP files.
# Attach the script instances to this singleton's scene to keep them alive.
func _initMods():
	var initScripts = []
	for modFSPath in _modZipFiles:
		var gdunzip = load('res://vendor/gdunzip.gd').new()
		gdunzip.load(modFSPath)
		for modEntryPath in gdunzip.files:
			var modEntryName = modEntryPath.get_file().to_lower()
			if modEntryName.begins_with('modmain') and modEntryName.ends_with('.gd'):
				var modGlobalPath = "res://" + modEntryPath
				Debug.l("ModLoader: Loading %s" % modGlobalPath)
				var packedScript = ResourceLoader.load(modGlobalPath)
				initScripts.append(packedScript)

	initScripts.sort_custom(self, "_compareScriptPriority")

	for packedScript in initScripts:
		Debug.l("ModLoader: Running %s" % packedScript.resource_path)
		var scriptInstance = packedScript.new(self)
		add_child(scriptInstance)


func _compareScriptPriority(a, b):
	var aPrio = a.get_script_constant_map().get("MOD_PRIORITY", 0)
	var bPrio = b.get_script_constant_map().get("MOD_PRIORITY", 0)
	if aPrio != bPrio:
		return aPrio < bPrio

	# Ensure that the result is deterministic, even when the priority is the same
	var aPath = a.resource_path
	var bPath = b.resource_path
	if aPath != bPath:
		return aPath < bPath

	return false


func installScriptExtension(childScriptPath:String):
	var childScript = ResourceLoader.load(childScriptPath)

	# Force Godot to compile the script now.
	# We need to do this here to ensure that the inheritance chain is
	# properly set up, and multiple mods can chain-extend the same
	# class multiple times.
	# This is also needed to make Godot instantiate the extended class
	# when creating singletons.
	# The actual instance is thrown away.
	childScript.new()

	var parentScript = childScript.get_base_script()
	var parentScriptPath = parentScript.resource_path
	Debug.l("ModLoader: Installing script extension: %s <- %s" % [parentScriptPath, childScriptPath])
	childScript.take_over_path(parentScriptPath)


func addTranslationsFromCSV(csvPath: String):
	var translationCsv = File.new()
	translationCsv.open(csvPath, File.READ)
	var TranslationParsedCsv = {}

	var translations = []

	# Load the header line
	var csvLine = translationCsv.get_csv_line()
	for i in range(1, csvLine.size()):
		var translationObject = Translation.new()
		translationObject.locale = csvLine[i]
		translations.append(translationObject)

	# Load translations
	while !translationCsv.eof_reached():
		csvLine = translationCsv.get_csv_line()
		if csvLine.size() == 1 and csvLine[0] == "":
			break  # Work around weird race condition in Godot leading to infinite loop
		var translationID = csvLine[0]
		for i in range(1, csvLine.size()):
			translations[i - 1].add_message(translationID, csvLine[i])

	translationCsv.close()

	# Install the translation objects
	for translationObject in translations:
		TranslationServer.add_translation(translationObject)


func appendNodeInScene(modifiedScene, nodeName:String = "", nodeParent = null, instancePath:String = "", isVisible:bool = true):
	var newNode
	if instancePath != "":
		newNode = load(instancePath).instance()
	else:
		newNode = Node.instance()
	if nodeName != "":
		newNode.name = nodeName
	if isVisible == false:
		newNode.visible = false
	if nodeParent != null:
		var tmpNode = modifiedScene.get_node(nodeParent)
		tmpNode.add_child(newNode)
		newNode.set_owner(modifiedScene)
	else:
		modifiedScene.add_child(newNode)
		newNode.set_owner(modifiedScene)

# Things to keep to ensure they are not garbage collected
var _savedObjects = []

func saveScene(modifiedScene, scenePath:String):
	var packed_scene = PackedScene.new()
	packed_scene.pack(modifiedScene)
	packed_scene.take_over_path(scenePath)
	_savedObjects.append(packed_scene)
