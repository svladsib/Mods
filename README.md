# Community Modding Repository for ΔV: Rings of Saturn

This repository contains:

- [Documentation for using and writing mods for ΔV](MODDING.md)
- [A copy of the mod loader script, included with the game](game/)
- [Source code](mods/) and [downloads](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/browse/mods?job=zip) for some mods:

| Name                                     | Author          | Summary                                                                          |
|------------------------------------------|-----------------|----------------------------------------------------------------------------------|
| [HeavyCothon](mods/HeavyCothon)          | harrygiel       | Adds a new ship variant, *OAM-Cothon*, with a high-stress hardpoint.             |
| [MaximumSpeed](mods/MaximumSpeed)        | harrygiel       | Increases the maximum allowed speed to 400m/s.                                   |
| [DiveClock](mods/DiveClock)              | CyberShadow     | Adds an in-game clock to OMS.                                                    |
| [AlertSounds](mods/AlertSounds)          | CyberShadow     | Plays different alert sounds depending on the cause.                             |
| [SalaryAlert](mods/SalaryAlert)          | CyberShadow     | Makes the "Crew" button in the Enceladus Prime menu red if there are unpaid staff. |
| [EnceladusPOIs](mods/EnceladusPOIs)      | CyberShadow     | Shows a list of <abbr title="Points of Interest">POIs</abbr> in Enceladus.       |
| [MysteryHunt](mods/MysteryHunt)          | CyberShadow     | Changes event volume music to depend to the proximity of the event.              |
| [ShowReliability](mods/ShowReliability)  | CyberShadow     | Shows equipment reliability data in the Equipment menu.                          |
| [ARMFocus](mods/ARMFocus)                | CyberShadow     | If an object is selected, makes the AR-1500 Manipulator focus on it.             |
| [TogglePropulsion](mods/TogglePropulsion) | CyberShadow     | Adds key binds to toggle all thrusters or main engines.                          |
| [rmIdLim](mods/rmIdLim)                  | Alfik0          | Significantly increases limit of identified objects e.g. chunks.                 |
| [relaxPerformanceThrottling](mods/relaxPerformanceThrottling) | Alfik0          | Allows controlling performance throttling.                                       |
| [YeetProtocol](mods/YeetProtocol)        | Alfik0          | Yeets away the chunks which fail the geologist check.                            |
