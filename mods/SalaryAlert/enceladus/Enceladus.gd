extends "res://enceladus/Enceladus.gd"

var _salaryAlert_crewButton

func _ready():
	_salaryAlert_crewButton = get_tree().root \
		.find_node("EnceladusMenu", true, false) \
		.find_node("Options", true, false) \
		.get_node("Crew")

func _process(delta):
	var haveUnpaid = false
	for fullName in CurrentGame.state.crew:
		var due = CurrentGame.getNextWages(fullName)
		var now = CurrentGame.getInGameTimestamp()
		if now > due:
			haveUnpaid = true
	if haveUnpaid:
		_salaryAlert_crewButton.modulate = Color(1,0,0)
	else:
		_salaryAlert_crewButton.modulate = Color(1,1,1)
