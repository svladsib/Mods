ShowReliability
===========

Shows equipment reliability data in the Equipment menu.

![screenshot](https://dump.cy.md/d2ce6762f4d70c2fe5bed439576a0992/1653484671.545853787.png)

Although computers and reactors have reliability data, they can't be seen using this mod, as you can't swap / upgrade computers and reactors (at this time). So, reliability data is shown only for thrusters and main engines.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/ShowReliability.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.
