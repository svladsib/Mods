ARMFocus
===========

If an object is selected, makes the AR-1500 Manipulator focus on it.

AR-1500 Manipulator behaves autonomously, as before, if nothing is selected or the autopilot is disengaged.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/ARMFocus.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.
