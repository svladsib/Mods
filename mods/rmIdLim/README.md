rmIdLim
===========

Significantly increases limit of identified objects e.g. chunks.

This mod effectively removes the limitation on the number of identified objects (chunks/ships/stations) through tenfold increase of limits. 

This resolves the issue of disappearing chunk identification in presence of their large number making drone filtering slighly more useful in case of aggresive mining.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/rmIdLim.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.

This mod creates configuration file `modConfig.txt` in the save file location present at:  
Windows: `%APPDATA%/dV`  
Mac: `~/Library/Application Support/dV`  
Linux: `~/.local/share/dV`  

`chunksMarkerMultiplier` float. default=100.0.  
This variable determines how many times the maximum number of chunks identified by geologists should be raised. The current(2022-07-05) default number in games is 50. Values of 2 means that the limit will be doubled.

`tacticalMarkerMultiplier` float. default=100.0.  
This variable determines how many times the maximum number of other markers should be raised. Probably irrelevant, but since we're here anyway...  The current(2022-07-05) default number in games is 20. Values of 2 means that the limit will be doubled.

In case of nonsensical values mod falls back on defaults.