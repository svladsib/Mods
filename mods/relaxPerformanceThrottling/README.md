relaxPerformanceThrottling
===========

Allows controlling performance throttling.

_O come all ye faithful in thy Mighty CPU_

This mod gives you power to control the performance throttling for asteroid spawning/despawning. It resolves issue of asteroids spawning in stripe pattern visible especially during aggresive mining (aggresive mining -> a lot of objects -> spawn throttled -> moving -> stripe of nothing -> :( ). 

By default the throttling is disabled.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/relaxPerformanceThrottling.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.

This mod creates configuration file `modConfig.txt` in the save file location present at:  
Windows: `%APPDATA%/dV`  
Mac: `~/Library/Application Support/dV`  
Linux: `~/.local/share/dV`  

`throttleMax` float. default=0.0.  
This variable determines the maximum throttling. Normal in game value is 0.8 and means that throttling will kill up to 80% of objects.

`collisionPairsMult` float. default=100.0.  
This is multiplier to value determining at what number of collision pairs throttling and asteroid despawning should increase. The default value of 100.0 means that limits is increased 100 times and is extremely unlikely to ever trigger.

`asteroidDespawnMinBoosted` bool. default=true.   
Double the minimum asteroid despawn distance. Should be irrelevant with high `collisionPairsMult`.

In case of nonsensical values mod falls back on defaults.