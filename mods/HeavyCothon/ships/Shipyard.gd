extends "res://ships/Shipyard.gd"

func _init():
	defaultShipConfig["HEAVY-COTHON"] = {
		"config": {
			"weaponSlot": {
				"right": {
					"type": "SYSTEM_EMD14"
				},
				"main": {
					"type": "SYSTEM_NONE"
				}
			},
			"reactor": {
				"power": 8.0
			},
			"ammo": {
				"capacity": 1000.0,
				"initial": 1000.0,
			},
			"fuel": {
				"capacity": 80000.0,
				"initial": 80000.0,
			},
			"capacitor": {
				"capacity": 500.0,
			},
			"turbine": {
				"power": 200.0,
			},
			"shielding": {
				"emp": 100,
			},
			"cargo": {
				"equipment": "SYSTEM_CARGO_STANDARD"
			},
			"autopilot": {
				"type": "SYSTEM_AUTOPILOT_MK1"
			},
			"propulsion": {
				"main": "SYSTEM_MAIN_ENGINE_PNTR",
				"rcs": "SYSTEM_THRUSTER_NDSTR"
			},
		}
	}

	usedShipConfigs["HEAVY-COTHON"] = [{
		"weaponSlot": {
			"right": {
				"type": "SYSTEM_EMD14"
			},
			"none": {
				"type": "SYSTEM_NONE"
			}
		},
		"reactor": {
			"power": 8.0
		},
		"ammo": {
			"capacity": 1000.0,
			"initial": 1000.0,
		},
		"fuel": {
			"capacity": 80000.0,
			"initial": 80000.0,
		},
		"capacitor": {
			"capacity": 500.0,
		},
		"turbine": {
			"power": 200.0,
		},
		"shielding": {
			"emp": 100,
		},
		"cargo": {
			"equipment": "SYSTEM_CARGO_STANDARD"
		},
		"autopilot": {
			"type": "SYSTEM_AUTOPILOT_MK1"
		},
		"propulsion": {
			"main": "SYSTEM_MAIN_ENGINE_PNTR",
			"rcs": "SYSTEM_THRUSTER_NDSTR"
		}
	}, {
		"weaponSlot": {
			"right": {
				"type": "SYSTEM_PDMWG-R"
			},
			"main": {
				"type": "SYSTEM_MWG"
			}
		},
		"reactor": {
			"power": 12.0
		},
		"ammo": {
			"capacity": 0.0,
			"initial": 0.0,
		},
		"fuel": {
			"capacity": 80000.0,
			"initial": 80000.0,
		},
		"capacitor": {
			"capacity": 500.0,
		},
		"turbine": {
			"power": 200.0,
		},
		"shielding": {
			"emp": 100,
		},
		"cargo": {
			"equipment": "SYSTEM_CARGO_STANDARD"
		},
		"autopilot": {
			"type": "SYSTEM_AUTOPILOT_MK2"
		},
		"propulsion": {
			"main": "SYSTEM_MAIN_ENGINE_PNTR",
			"rcs": "SYSTEM_THRUSTER_MA150HO"
		}
	}]

func _ready():
	ships["HEAVY-COTHON"] = preload("res://HeavyCothon/ships/HeavyCothon.tscn")
