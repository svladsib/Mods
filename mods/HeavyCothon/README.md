HeavyCothon
===========

Adds a new ship variant, *OAM-Cothon*, with a high-stress hardpoint.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/HeavyCothon.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.

The ship can be bought new or used from the Dealer.
(You may need to fast-forward time for a week or two to get the ship to appear in the used ships list.)
