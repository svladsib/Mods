extends Node

func _init(modLoader = ModLoader):
	Debug.l("BetterRefining: Initializing")
	modLoader.installScriptExtension("res://BetterRefining/ships/modules/MineralProcessingUnit.gd")
	Debug.l("BetterRefining: Initialized")
