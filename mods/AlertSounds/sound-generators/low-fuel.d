module low_fuel;

import std.algorithm;
import std.conv;
import std.file;
import std.random;
import std.range;
import std.stdio;

import core.thread;
import core.time;

import ae.utils.sound.asound;
import ae.utils.sound.riff;
import ae.utils.sound.wave;

enum sampleRate = 44100;

void main(string[] args)
{
	auto wave = makeWave();
	enum length = 1 * sampleRate;
	std.file.write(args[0] ~ ".wav", makeRiff(wave.take(length), sampleRate).array());
	std.file.write(args[0] ~ ".pcm", wave.take(length).array);
	playWave(wave, sampleRate);
}

auto makeWave()
{
	return iota(size_t.max)
		.map!(delegate short (n) {
			enum duration = sampleRate ;
			auto phase = (n % duration) / float(duration);
			return cast(short)(
				sineWave!short(200 + phase * 100)[n % duration]
				* 0.8
				* (1-(1-phase)^^99) // fix clipping at start
				* (1 - phase) // fade out
			);
		});
}
