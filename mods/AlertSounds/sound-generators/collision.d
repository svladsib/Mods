import std.algorithm;
import std.conv;
import std.file;
import std.random;
import std.range;

import core.thread;
import core.time;

import ae.utils.sound.asound;
import ae.utils.sound.riff;
import ae.utils.sound.wave;

enum sampleRate = 44100;

void main(string[] args)
{
	auto wave = makeWave();
	enum length = 1 * sampleRate;
	std.file.write(args[0] ~ ".wav", makeRiff(wave.take(length), sampleRate).array());
	std.file.write(args[0] ~ ".pcm", wave.take(length).array);
	playWave(wave, sampleRate);
}

auto makeWave()
{
	return iota(size_t.max)
		.map!(delegate short (n) {
			enum duration = sampleRate / 6;
			auto part = (n % duration) / (duration / 2);
			auto phase = (n % (duration / 2)) / float(duration / 2);
			switch (part)
			{
				case 0:
					return cast(short)(
						(
							sineWave!short(sampleRate / 500)[n]
							/ 2
							+
							triangleWave!short(sampleRate / 200)[n] / 3
						)
						// fix clipping
						* (1-phase^^9)
						* (1-(1-phase)^^9)
					) / 4;
				case 1:
					return 0;
				default: return 0;
			}
		});
}
