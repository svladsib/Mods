DiveClock
===========

Adds an in-game clock to OMS.

![screenshot](https://dump.cy.md/c1395d76b592d3a35b0557134cc747c5/1653329865.929205029.png)

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/DiveClock.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.

The clock is added to the top right corner of the On-board Maintenance System, and displays the current in-game date and time, as well as the in-game time spent in the current dive.
