extends Node

const MOD_PRIORITY = -1

func _init(modLoader = ModLoader):
	modLoader.installScriptExtension("res://MysteryHunt/Music.gd")
	modLoader.installScriptExtension("res://MysteryHunt/CurrentGame.gd")
	modLoader.installScriptExtension("res://MysteryHunt/ships/ship-ctrl.gd")

	_patchFile("res://AsteroidSpawner.gd")
	_patchFile("res://ships/ship-ctrl.gd")

# To avoid hard-coding scripts which generate mood,
# scan all game code for scripts which contain the "moodChange" string,
# and patch those dynamically.

func _ready():
	_scanDir("res://")

func _scanDir(path):
	var dir = Directory.new()
	if dir.open(path) != OK:
		Debug.l("MysteryHunt: Failed to open " + path)
		return
	if dir.list_dir_begin() != OK:
		Debug.l("MysteryHunt: Failed to read " + path)
		return
	while true:
		var file_name = dir.get_next()
		if file_name == "":
			break
		var full_name = path.plus_file(file_name)
		if dir.current_is_dir():
			_scanDir(full_name)
		else:
			_scanFile(full_name)

func _scanFile(path):
	if !path.ends_with(".gd") and !path.ends_with(".gdc"):
		return
	if path.begins_with("res://MysteryHunt/"):
		return
	if path.begins_with("res://ships/ship-ctrl.gd"):
		return  # already patched
	if !path.begins_with("res://easters/") and \
		!path.begins_with("res://ships/") and \
		!path.begins_with("res://story/"):
		return

	var file = File.new()
	file.open(path, File.READ)
	var bytecode = file.get_buffer(file.get_len())
	file.close()

	var bytecodeStr = bytecode.hex_encode()

	if "moodChange".to_ascii().hex_encode() in bytecodeStr:
		# Debug.l("MysteryHunt: Replacing " + path)  # Big spoilers!

		_patchFile(path)


func _patchFile(parentScriptPath):
	var file = File.new()
	file.open("res://MysteryHunt/Hook.tpl.gd", File.READ)
	var childTemplate = file.get_as_text()
	file.close()

	var childSource = childTemplate.replace("%PARENT%", parentScriptPath)

	var childScript = GDScript.new()
	childScript.set_source_code(childSource)
	childScript.reload()
	childScript.new()  # Force compilation
	childScript.take_over_path(parentScriptPath)
