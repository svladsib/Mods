extends "res://Music.gd"

func _on_moodChange(mood, delta):
	._on_moodChange(mood, delta)

	if CurrentGame.mysteryHunt_last and CurrentGame.mysteryHunt_player and CurrentGame.mysteryHunt_last[1] == delta:
		var source = CurrentGame.mysteryHunt_last[0]
		var us     = CurrentGame.mysteryHunt_player
		var distance = (source - us).length()
		# Debug.l("Distance from %s to %s: %s" % [CurrentGame.mysteryHunt_player, CurrentGame.mysteryHunt_last[0], distance])
		targetMood[mood] = clamp(1 - distance/20000,0,1)
