MysteryHunt
===========

Changes event volume music to depend to the proximity of the event.

When there is an event in your ship's vicinity, the game normally plays music at a constant volume. This mod changes the volume (and intensity of screen effects) to depend on how close you are to objects which cause this music to be played.

By paying attention to the volume of the music, it becomes possible to eventually find its source, instead of relying on luck.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/MysteryHunt.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.
